## 合作任务

1. 账户类型字典
    1. 个人账户
    1. 机构账户
    1. 子账户
1. 最高到国家的行征区字典接口
1. 职业类型字典
    1. 通用
    1. 医生
    1. 药师
    1. 其他
1. 认证类型字典
    * 个人认证类型
        1. 个人身份认证
        1. 医师资格认证
        1. 药师资格认证
    * 组织机构认证类型
        1. 机构信息信息
        1. 医疗机构资格认证



## Usage

A Makefile is included for convenience

Build the binary

```
make build
```

Run the service
```
./dict-srv
```

Build a docker image
```
make docker
```