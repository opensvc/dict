package database

import (
	"errors"
	"github.com/jinzhu/gorm"
	"github.com/pku-hit/dict/model/entity"
	"github.com/micro/go-micro/util/log"
	"github.com/pku-hit/dict/proto"
	"github.com/pku-hit/dict/util"
	"time"
)

func WithId(id string) (dict *entity.DictInfo, err error) {
	dict = &entity.DictInfo{}
	err = db.Where("parent_id = ?", id).Find(dict).Error
	if err == gorm.ErrRecordNotFound {
		dict = nil
	}
	return
}
func GetAccount(parent_id string) (dicts []*entity.DictInfo, err error) {
	query := db.New()
	if !util.String.IsEmptyString(parent_id) {
		query = query.Where("parent_id = ?", parent_id)
	}
	//query.Where("Type = ? and ParentId is null", proto.DictType_Root.String())
	err = query.Find(&dicts).Error
	if err != nil {
		log.Error(util.Json.ToJsonString(err))
	} else {
		log.Info(util.Json.ToJsonString(dicts))
	}
	return
}
func NewAcDict(category, parentId, code, name, pyCode string, dictType proto.AcDictType, value interface{}) (dict *entity.DictInfo, err error) {



	now := time.Now()
	dict = &entity.DictInfo{
		ID:       util.Snowflake.GenId(),
		Code:     code,
		Name:     name,
		PyCode:   pyCode,
		Value:    util.Json.ToJsonString(value),
		Status:   proto.DictStatus_Normal.String(),
		CreateAt: &now,
		UpdateAt: &now,
	}

	if util.String.IsEmptyString(parentId) {
		if dictType != proto.AcDictType_Root {
			err = errors.New("没有指定ParentId的字典，限制仅允许为Root类型")
			return
		}
		dict.Type = proto.AcDictType_Root.String()
	} else {
		if dictType != proto.AcDictType_Node && dictType != proto.AcDictType_Group {
			err = errors.New("指定ParentId的字典，限制仅允许为Group或Node类型")
			return
		}
		if _, err = ExistDictWithId(parentId); err == gorm.ErrRecordNotFound {
			err = errors.New("指定的父节点不存在")
			return
		}
		dict.ParentId = parentId
		dict.Type = dictType.String()
	}

	if !util.String.IsEmptyString(category) {
		dict.Category = category
	}

	err = db.Save(dict).Error
	if err != nil {
		log.Error("save new dict error %s.", err.Error())
	}
	return
}