package handler

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/micro/go-micro/util/log"
	"github.com/pku-hit/dict/component/database"
	"github.com/pku-hit/dict/model"

	"github.com/pku-hit/libresp"
)

type Region struct{}

func (e *Region) ListCountry(ctx context.Context, req *empty.Empty, resp *libresp.ListResponse) error {
	log.Log("Received Region.ListCountry request")
	dicts, err := database.ListRoot("")
	if err != nil {
		resp.GenerateListResponseWithInfo(libresp.GENERAL_ERROR, err.Error())
		return nil
	}
	resp.GenerateListResponseSucc(model.GetDictsAny(dicts))
	return nil
}
